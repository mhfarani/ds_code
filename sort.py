
def selection_sort(non_sort_array: list):
    length = len(non_sort_array)

    for i in range(length):
        min_index = i
        for j in range(i+1, length):
            if non_sort_array[j] < non_sort_array[min_index]:
                min_index = j
        non_sort_array[i], non_sort_array[min_index] = non_sort_array[min_index], non_sort_array[i]
    print(non_sort_array)


def insertion_sort(array: list):
    size = len(array)
    for i in range(1, size):
        flag = array[i]
        j = i - 1
        while flag < array[j] and j >= 0:
            array[j + 1] = array[j]
            j -= 1
        array[j + 1] = flag
    print(array)


def bubble_sort(array: list):
    size = len(array)
    for i in range(size):
        for k in range(size-1):
            if array[k] > array[k + 1]:
                array[k+1], array[k] = array[k], array[k+1]
        size = size - 1

    print(array)

